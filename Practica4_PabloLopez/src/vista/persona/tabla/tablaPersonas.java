/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista.persona.tabla;

import controlador.tda.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;
import modelo.Persona.Persona;

/**
 *
 * @author Usuario
 */
public class tablaPersonas extends AbstractTableModel {
    private ListaEnlazadaServices<Persona> lista;

    public ListaEnlazadaServices<Persona> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Persona> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getColumnCount(){
        return 5;
    }
    
    @Override
    public int getRowCount(){
        return lista.getSize();
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0: return "Nro";
            case 1: return "Apellidos";
            case 2: return "Nombre";
            case 3: return "Cedula";
            case 4: return "FechaNacimiento";
            default: return null; 
        }
    }
    
    @Override
    public Object getValueAt(int arg0, int arg1){
        Persona persona = lista.obtenerDato(arg0);
        switch(arg1){
            case 0: return (arg0+1);
            case 1: return persona.getApellidos();
            case 2: return persona.getNombres();
            case 3: return persona.getCedula();
            case 4: return persona.getFechaNacimiento();
            default: return null; 
        }
    }
}
